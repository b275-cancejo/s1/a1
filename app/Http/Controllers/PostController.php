<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
// to access with the authenticated user
use Illuminate\Support\Facades\Auth;
// to have access with queries related to the Post Entity/Model
use App\Models\Post;
use App\Models\PostLike;
use App\Models\PostComment;

class PostController extends Controller
{
    //action to return a view containing a form for post creation
    public function create(){    
        return view('posts.create');  
    }

    // action to received the form data and subsequently store said data in post table
    public function store(Request $request){
        // if there is an authenticated user
        if(Auth::user()){
            //instantiate a new post object from the Post model
            $post = new Post;
            //define the properties of the $post object using the received form data.
            $post->title = $request->input('title');
            $post->content = $request->input('content');
            // get the id of the authenticated user and set it as the foreign key (user_id)
            $post->user_id = (Auth::user()->id);
            // save this post object in the database
            $post->save();

            return redirect('/posts');
        }
        else{
            return redirect('/login');
        }
    }

    // action that will return a view showing all blog posts
    public function index(){
        $posts = Post::all()->where('isActive', true);
        // The "with()" method will allows us to pass information from the controller to view page.
        return view('posts.index')->with('posts', $posts);
    }
    //Activity for s02
    public function welcome(){

       $posts = Post::inRandomOrder()->where('isActive', true)
                    ->limit(3)
                    ->get();
       return view('welcome')->with('posts', $posts);
    }

    // action to show only the post authored by the authenticated user.
    public function myPost(){
        if(Auth::user()){
            //We are able to fetch the posts related to a specific user because of the establish relationship between the models.
            $posts = Auth::user()->posts;

            return view('posts.index')->with('posts', $posts);
        }
        else{
            return redirect('/login');
        }
    }

    // action that will return a view showing a specific post using the URL parameter $id to query for the database entry to be shown.
    public function show($id){
        $post = Post::find($id);

        return view('posts.show')->with('post', $post);
    }
    

    // Activity 03
    public function edit($id){
        if(Auth::user()){
            $post = Post::find($id);
            if(Auth::user()->id != $post->user_id){
                return redirect('/');
            }
            else{
                return view('posts.edit')->with('post', $post);  
            }
        }
        else{
            return redirect('/login');
        }
        
        
    }

    // action to overwrite an existing post when the Put route with matching URL parameter ID is accessed.
    public function update(Request $request, $id){
        $post = Post::find($id);
        // if authenticated user's ID is the same with the post's user_id.
        if(Auth::user()->id == $post->user_id){
            $input = $request->all();
            $post->update($input);
        }
        return redirect('/posts');    
    }

    // action to remove a post with the matching URL id parameter.
    public function archive($id){
        $post = Post::find($id);
        if(Auth::user()->id == $post->user_id){
            $post->isActive = false;
            $post->save();
        }
        return redirect('/posts');
    }

    // action that will allow a authenticated user who is not the post author to toggle a like/unlike on the post being viewed.

    public function likes($id){
        $post = Post::find($id);

        if(Auth::user()){
            $user_id = Auth::user()->id;
            // if the authenticated user is not the post owner.
            if($post->user_id != $user_id){
                // check if a post like has been made 
                if($post->likes->contains("user_id", $user_id)){
                    // delete the like made by the user to unlike this post.
                    PostLike::where('post_id', $post->id)
                    ->where('user_id', $user_id)
                    ->delete();
                }
                // create a new like record in the post_likes table with the user_id and post_id
                else{
                    $postLike = new PostLike;
                    $postLike->post_id = $post->id;
                    $postLike->user_id = $user_id;
                    $postLike->save();
                }
            }
            
            return redirect("/posts/$id");
            
        }
        else{
            return redirect('/login');
        }
    }

    public function comments(Request $request, $id){
        $post = Post::find($id);

        if(Auth::user()){
            $comment = new PostComment;
            
            $comment->content = $request->input('content');
            $comment->post_id = $post->id;
            $comment->user_id = (Auth::user()->id);
            $comment->save();

            return view('posts.show')->with('comments', $comment);
        }

    }
    

}
