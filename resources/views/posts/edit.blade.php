@extends('layouts.app')

@section('content')
 
            <div class="card-footer">
                <form method="POST" action="/posts/{{$post->id}}">
                    @method('PUT')
                    @csrf
                    <div class="form-group">
                        <label for="title">Title:</label>
                        <input type="text" name="title" id="title" class="form-control" value="{{$post->title}}">
                    </div>
            
                    <div class="form-group">
                        <label for="content">Content:</label>
                        <textarea name="content" id="content" class="form-control">{{$post->content}}</textarea>
                    </div>
                    
                    
                    <div class="mt-2">
                        <button type="submit" class="btn btn-primary">Update Post</button>
                    </div>
                </form>
            </div>
	
@endsection