@extends('layouts.app')

@section('content')
	<div class="card">
		<div class="card-body">
			<h2 class="card-title">{{$post->title}}</h2>
			<p class="card-subtitle text-muted">Author: {{$post->user->name}}</p>
			<p class="card-subtitle text-muted">Created at: {{$post->created_at}}</p>
			<p class="card-text">{{$post->content}}</p>

			@if(Auth::user())
				@if(Auth::id() != $post->user_id)
					<form class="d-inlike" method="POST" action="/posts/{{$post->id}}/like">
						@method('PUT')
						@csrf
						@if($post->likes->contains("user_id", Auth::id()))
							<button type="submit" class="btn btn-danger">Unlike</button>
							
						@else
							<button type="submit" class="btn btn-success">Like</button>
							
						@endif
						<!-- Button trigger modal -->
						<button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#exampleModal">
							Comment
						</button>
					</form>
				@endif
			@endif
			<div class="mt-3">
				<a href="/posts" class="card-link">View All Posts</a>
				
			</div>
		</div>

		{{-- Comment Section --}}
	@if(Auth::user())
		@if($comments != null)
			@foreach($comments as $comment)
				<div class="card-body">
					<h4 class="card-title mb-3 text-center">{{$comment->content}}</h4>

					<h6 class="card-text mb-3">
                        Posted By: {{$comment->user->name}}
                    </h6>
				</div>
			@endforeach
		@endif
	@endif
		
		{{-- Modal for posting comments --}}
	
		<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					</form method="POST" action="/posts/{{$post->id}}/comments">
						@method('PUT')
						@csrf
						<div class="modal-header">
							<h5 class="modal-title" id="exampleModalLabel">Leave a comment</h5>
							<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
						</div>
						<div class="modal-body">
							<textarea id="content" name="content" class="w-100"></textarea>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
							<button type="submit" class="btn btn-primary">Add comment</button>
						</div>
					</form>
				</div>
			</div>
		</div>
		
	</div>
@endsection