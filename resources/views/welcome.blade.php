{{-- Activity for s02 --}}
@extends('layouts.app')

@section('content')
    <div class="mb-5">
        <img src="https://cdn.freebiesupply.com/logos/large/2x/laravel-1-logo-png-transparent.png" class="img-fluid">   
    </div>
    <h2 class="text-center mb-3">Featured Posts:</h2>
        @if(count($posts) > 0)
        @foreach($posts as $post)
            <div class="card text-center my-2">
                <div class="card-body">
                    <h4 class="card-title mb-3">
                        <a href="/posts/{{$post->id}}">
                            {{$post->title}}
                        </a>
                    </h4>
                    {{-- eloquent have a specific functionality that automatically joins table, once the relationship is identified in the Models. --}}
                    <h6 class="card-text mb-3">
                        Author: {{$post->user->name}}
                    </h6>
              
                    @if(Auth::user())
                        @if(Auth::user()->id == $post->user_id)
                            <div class="card-footer">
                                <form method="POST" action="/posts/{{$post->id}}">
                                    <a href="/posts/{{$post->id}}/edit" class="btn btn-primary">Edit Post</a>

                                    @method('DELETE')
                                    @csrf
                                    <button type="submit" class="btn btn-danger">Delete Post</button>
                                </form>
                            </div>
                        @endif
                    @endif
                </div>
            </div>
        @endforeach
    @else
        <div>
            <h2>There are no post to show.</h2>
            <a href="/posts/create" class="btn btn-info">Create post</a>
        </div>
    @endif

@endsection